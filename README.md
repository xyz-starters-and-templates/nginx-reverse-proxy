# Reverse Proxy

This project was used to merge multi source domains into signle outbound domain by using [nginx](https://www.nginx.com/).

## Usage

- Merge multi domain resources into one **domain**
- Simple redirection setup

## Requirment
- Make sure you have install [Docker](https://www.docker.com/get-started/) and its CLI

## Get Started

1. Create a `.env` file with the following command.
```
mv .env.example .env
```
2. Add your outbound domain `DOMAIN` and source domains `SOURCE_1` and `SOURCE_2`.
3. Update your redirection map in `/proxy/nginx/redirect.map`.
4. Run the following command.
```
docker-compose up --build
```

## Build your own Docker image

```
cd proxy/
docker build --build-arg DOMAIN=$DOMAIN --build-arg SOURCE_1=$SOURCE_1 --build-arg SOURCE_2=$SOURCE_2  -t your_image_name:your_tag_name --no-cache .
```


## License
License: MIT
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
